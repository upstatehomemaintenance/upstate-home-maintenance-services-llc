We are a licensed, insured, and bonded HVAC company serving Upstate SC. We offer quality, dependable service for all brands of heating and air conditioning systems. We offer 10-year labor plans and financing is available.

Upstate Home Maintenance Services is not your average HVAC contractor.

Address: 700 Otts Shoals Rd, Roebuck, SC 29376, USA

Phone: 864-529-7310

Website: https://heatingandairspartanburgsc.com
